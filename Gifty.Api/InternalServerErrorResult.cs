﻿using Microsoft.AspNetCore.Mvc;

namespace Gifty.Api
{
    public class InternalServerErrorResult : StatusCodeResult
    {
        public InternalServerErrorResult() : base(500)
        {
        }
    }
}
