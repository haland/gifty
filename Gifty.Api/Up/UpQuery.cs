﻿using Gifty.Contracts.Up;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Gifty.Api.Up
{
    [Route("api/v1/up")]
    [ApiController]
    public class UpQuery : ControllerBase
    {
        [HttpGet]
        [ProducesResponseType(200, Type = typeof(UpDto))]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get()
        {
            await Task.CompletedTask;
            return Ok(new UpDto());
        }
    }
}
