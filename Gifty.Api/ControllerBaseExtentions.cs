﻿using Microsoft.AspNetCore.Mvc;

namespace Gifty.Api
{
    public static class ControllerBaseExtentions
    {
        public static InternalServerErrorResult InternalServerError(this ControllerBase controller)
        {
            return new InternalServerErrorResult();
        }
    }
}
