﻿using System;
using System.Threading.Tasks;
using Gifty.Contracts.Persons;
using Gifty;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Gifty.Api.Persons
{
    [Route("api/v1/persons")]
    [ApiController]
    public class GetPersonQuery : ControllerBase
    {
        private readonly GiftyContext _context;
        private readonly ILogger<GetPersonQuery> _logger;

        public GetPersonQuery(GiftyContext context, ILogger<GetPersonQuery> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(GetPersonDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                _logger.LogDebug("Querying database for user with id {PersonId}", id);

                var person = await _context.Persons
                    .AsNoTracking()
                    .SingleOrDefaultAsync(p => p.Id == id);

                if (person == null)
                {
                    return NotFound();
                }

                _logger.LogDebug("Person with id {PersonId} found", id);

                var result = new GetPersonDto
                {
                    Id = person.Id,
                    Name = person.Name
                };

                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError("An error occurred while getting person with id {PersonId}. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    id, ex.Message, ex.StackTrace);

                return this.InternalServerError();
            }
        }
    }
}
