﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Gifty.Contracts.Persons;
using Gifty;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Gifty.Api.Persons
{
    [Route(Route)]
    [ApiController]
    public class UpsertPersonCommand : ControllerBase
    {
        private const string Route = "api/v1/persons";

        private readonly GiftyContext _context;
        private readonly ILogger<UpsertPersonCommand> _logger;

        public UpsertPersonCommand(GiftyContext context, ILogger<UpsertPersonCommand> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(201, Type = typeof(CreatedPersonDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> Post([FromBody][Required] UpsertPersonDto dto)
        {
            try
            {
                if (dto.Id.HasValue)
                {
                    return await Update(dto);
                }

                return await Create(dto);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occurred while upserting person with id {PersonId}. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    dto.Id, ex.Message, ex.StackTrace);

                return this.InternalServerError();
            }
        }

        private async Task<IActionResult> Create(UpsertPersonDto dto)
        {
            var person = _context.Persons.Add(new Person
            {
                Name = dto.Name
            });

            await _context.SaveChangesAsync();

            return Created($"{Route}/{nameof(UpsertPersonCommand)}", new CreatedPersonDto
            {
                Id = person.Entity.Id
            });
        }

        private async Task<IActionResult> Update(UpsertPersonDto dto)
        {
            var person = await _context.Persons
                .SingleOrDefaultAsync(p => p.Id == dto.Id.Value);

            if (person == null)
            {
                return NotFound();
            }

            person.Name = dto.Name;
            _context.Persons.Update(person);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
