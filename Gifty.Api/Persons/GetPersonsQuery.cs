﻿using Gifty.Contracts.Persons;
using Gifty;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Persons
{
    [Route("api/v1/persons")]
    [ApiController]
    public class GetPersonsQuery : ControllerBase
    {
        private readonly GiftyContext _context;
        private readonly ILogger<GetPersonsQuery> _logger;

        public GetPersonsQuery(GiftyContext context, ILogger<GetPersonsQuery> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(GetPersonsDto))]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var persons = await _context.Persons
                .AsNoTracking()
                .ToListAsync();

                var result = new GetPersonsDto
                {
                    Persons = persons.Select(p => new GetPersons_PersonDto
                    {
                        Id = p.Id,
                        Name = p.Name
                    }).ToList()
                };

                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError("An error occurred while getting persons. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    ex.Message, ex.StackTrace);
                return this.InternalServerError();
            }
        }
    }
}
