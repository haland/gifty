﻿using Gifty.Contracts.Events;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Events
{
    [Route("api/v1/events")]
    [ApiController]
    public class GetEventQuery : ControllerBase
    {
        private readonly GiftyContext _context;
        private readonly ILogger<GetEventQuery> _logger;

        public GetEventQuery(GiftyContext context, ILogger<GetEventQuery> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(GetEventDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var @event = await _context.Events
                    .Include(g => g.Gifts)
                    .SingleOrDefaultAsync(g => g.Id.Equals(id));

                if (@event == null)
                {
                    return NotFound();
                }

                var response = new GetEventDto
                {
                    Id = @event.Id,
                    Description = @event.Description,
                    Gifts = @event.Gifts.Select(g => new GetEvent_GiftDto
                    {
                        Id = g.Id
                    }).ToList()
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occurred while getting event with id {EventId}. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    id, ex.Message, ex.StackTrace);
                return this.InternalServerError();
            }
        }
    }
}
