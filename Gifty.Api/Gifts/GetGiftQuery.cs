﻿using Gifty.Contracts.Gifts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Gifts
{
    [Route("api/v1/gifts")]
    [ApiController]
    public class GetGiftQuery : ControllerBase
    {
        private readonly GiftyContext _context;
        private readonly ILogger<GetGiftQuery> _logger;

        public GetGiftQuery(GiftyContext context, ILogger<GetGiftQuery> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(GetGiftDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get(Guid id)
        {
            try
            {
                var gift = await _context.Gifts
                    .Include(g => g.Donors)
                    .ThenInclude(d => d.Donor)
                    .Include(g => g.Recipients)
                    .ThenInclude(r => r.Recipient)
                    .SingleOrDefaultAsync(g => g.Id.Equals(id));

                if (gift == null)
                {
                    return NotFound();
                }

                var response = new GetGiftDto
                {
                    Id = gift.Id,
                    Description = gift.Description,
                    Donors = gift.Donors.Select(gd => new GetGift_DonorDto
                    {
                        Id = gd.Donor.Id,
                        Name = gd.Donor.Name
                    }).ToList(),
                    Recipients = gift.Recipients.Select(gr => new GetGift_RecipientDto
                    {
                        Id = gr.Recipient.Id,
                        Name = gr.Recipient.Name
                    }).ToList()
                };

                return Ok(response);
            }
            catch(Exception ex)
            {
                _logger.LogError("An error occurred while getting gift with id {GiftId}. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    id, ex.Message, ex.StackTrace);
                return this.InternalServerError();
            }
        }
    }
}
