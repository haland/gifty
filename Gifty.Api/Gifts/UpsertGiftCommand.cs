﻿using Gifty.Contracts.Gifts;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Gifts
{
    [Route(Route)]
    [ApiController]
    public class UpsertGiftCommand : ControllerBase
    {
        private const string Route = "api/v1/gifts";

        private readonly GiftyContext _context;
        private readonly ILogger<UpsertGiftCommand> _logger;

        public UpsertGiftCommand(GiftyContext context, ILogger<UpsertGiftCommand> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(201, Type = typeof(CreatedGiftDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [Consumes("application/json")]
        [Produces("application/json")]
        public async Task<IActionResult> Post([FromBody][Required] UpsertGiftDto dto)
        {
            try
            {
                var validationResult = await Validate(dto);
                if (validationResult != null)
                {
                    return validationResult;
                }

                if (dto.Id.HasValue)
                {
                    return await Update(dto);
                }

                return await Create(dto);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occurred while upserting gift with id {GiftId}. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    dto.Id, ex.Message, ex.StackTrace);
                return this.InternalServerError();
            }
        }

        private async Task<IActionResult> Create(UpsertGiftDto dto)
        {
            var gift = _context.Gifts.Add(new Gift
            {
                Description = dto.Description
            });

            await AddOrUpdateDonorGifts(gift.Entity, dto.Donors);
            await AddOrUpdateRecipientGifts(gift.Entity, dto.Recipients);

            await _context.SaveChangesAsync();

            return Created($"{Route}/{nameof(UpsertGiftCommand)}", new CreatedGiftDto
            {
                Id = gift.Entity.Id
            });
        }

        private async Task<IActionResult> Update(UpsertGiftDto dto)
        {
            var gift = await _context.Gifts
                .SingleOrDefaultAsync(g => g.Id.Equals(dto.Id));

            if (gift == null)
            {
                return NotFound();
            }

            gift.Description = dto.Description;

            _context.Gifts.Update(gift);

            await AddOrUpdateDonorGifts(gift, dto.Donors);
            await AddOrUpdateRecipientGifts(gift, dto.Recipients);

            await _context.SaveChangesAsync();

            return Ok();
        }

        private async Task AddOrUpdateDonorGifts(Gift gift, List<UpsertGift_DonorDto> donorDtos)
        {
            var donorIds = donorDtos.Select(d => d.Id);
            var existingDonors = await _context.DonorGifts.Where(dg => dg.GiftId.Equals(gift.Id)).ToListAsync();

            if (existingDonors.Any())
            {
                _context.DonorGifts.RemoveRange(existingDonors);
            }

            if (donorIds.Any())
            {
                var donors = await _context.Persons.Where(d => donorIds.Contains(d.Id)).ToListAsync();

                _context.DonorGifts.AddRange(donors.Select(donor => new DonorGift { Donor = donor, Gift = gift }));
            }
        }

        private async Task AddOrUpdateRecipientGifts(Gift gift, List<UpsertGift_RecipientDto> recipientDtos)
        {
            var recpientIds = recipientDtos.Select(d => d.Id);
            var existingRecipients = await _context.RecipientGifts.Where(rg => rg.GiftId.Equals(gift.Id)).ToListAsync();

            if (existingRecipients.Any())
            {
                _context.RecipientGifts.RemoveRange(existingRecipients);
            }

            if (recpientIds.Any())
            {
                var recipients = await _context.Persons.Where(d => recpientIds.Contains(d.Id)).ToListAsync();

                _context.RecipientGifts.AddRange(recipients.Select(recipient => new RecipientGift { Recipient = recipient, Gift = gift }));
            }
        }

        private async Task<IActionResult> Validate(UpsertGiftDto dto)
        {
            var validationMessages = new List<string>();

            if (await InvalidPersonIds(dto.Donors.Select(d => d.Id)))
            {
                validationMessages.Add("Donor list contains invalid ids");
            }

            if (await InvalidPersonIds(dto.Recipients.Select(r => r.Id)))
            {
                validationMessages.Add("Recipient list contains invalid ids");
            }

            if (validationMessages.Any())
            {
                return BadRequest(string.Join(Environment.NewLine, validationMessages));
            }

            return null;
        }
        
        private async Task<bool> InvalidPersonIds(IEnumerable<Guid> ids)
        {
            if (ids.Any())
            {
                return false;
            }

            var numberOfFoundPersons = await _context.Persons.CountAsync(p => ids.Contains(p.Id));

            return ids.Count() != numberOfFoundPersons;
        }
    }
}
