﻿using Gifty.Contracts.Gifts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Gifts
{
    [Route("api/v1/gifts")]
    [ApiController]
    public class GetGiftsQuery : ControllerBase
    {
        private readonly GiftyContext _context;
        private readonly ILogger<GetGiftsQuery> _logger;

        public GetGiftsQuery(GiftyContext context, ILogger<GetGiftsQuery> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(GetGiftsDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [Produces("application/json")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var gifts = await _context.Gifts
                    .Include(g => g.Donors)
                    .ThenInclude(d => d.Donor)
                    .Include(g => g.Recipients)
                    .ThenInclude(r => r.Recipient)
                    .ToListAsync();

                var response = new GetGiftsDto
                {
                    Gifts = gifts.Select(g => new GetGifts_GiftDto
                    {
                        Id = g.Id,
                        Description = g.Description,
                        Donors = g.Donors.Select(d => new GetGifts_DonorDto
                        {
                            Id = d.Donor.Id,
                            Name = d.Donor.Name
                        }).ToList(),
                        Recipients = g.Recipients.Select(r => new GetGifts_RecipientDto
                        {
                            Id = r.Recipient.Id,
                            Name = r.Recipient.Name
                        }).ToList()
                    }).ToList()
                };

                return Ok(response);
            }
            catch (Exception ex)
            {
                _logger.LogError("An error occurred while getting gifts. " +
                    "Exception:\n{ExceptionMessage}\n{ExceptionStackTrace}",
                    ex.Message, ex.StackTrace);
                return this.InternalServerError();
            }
        }
    }
}
