# Gifty

## Introduction

Todo...


## Getting started

Install `Docker for Windows`. You can find it (here)[https://www.docker.com/].

Restart your computer

Create an external switch in Hyper-V following the guide (here)[https://blogs.technet.microsoft.com/canitpro/2014/03/10/step-by-step-enabling-hyper-v-for-use-on-windows-8-1/]

Install `chocolatey` by running the following command from a Powershell window as administrator:
`Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))`

Open a new Powershell window as administrator and execute the following commands:
`minikube start --vm-driver hyperv`