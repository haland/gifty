﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Gifty
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseConfiguration(new ConfigurationBuilder()
                    .AddCommandLine(args)
                    .Build())
                .UseStartup<Startup>()
                .ConfigureLogging(ConfigureLogging);

        private static void ConfigureLogging(ILoggingBuilder loggingBuilder)
        {
            loggingBuilder.AddFilter((category, logLevel) =>
            {
                // Ignore log level lower than Information for Kestrel http connection
                // See: https://github.com/aspnet/KestrelHttpServer/issues/1853
                // See: https://github.com/aspnet/KestrelHttpServer/issues/2941

                if (category.Contains("HttpsConnectionAdapter"))
                {
                    if (logLevel == LogLevel.Trace || logLevel == LogLevel.Debug || logLevel == LogLevel.None)
                    {
                        return false;
                    }
                }

                return true;
            });
        }
    }
}
