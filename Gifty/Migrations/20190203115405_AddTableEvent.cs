﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Gifty.Migrations
{
    public partial class AddTableEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "EventId",
                table: "Gifts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Events",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Events", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Gifts_EventId",
                table: "Gifts",
                column: "EventId");

            migrationBuilder.AddForeignKey(
                name: "FK_Gifts_Events_EventId",
                table: "Gifts",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Gifts_Events_EventId",
                table: "Gifts");

            migrationBuilder.DropTable(
                name: "Events");

            migrationBuilder.DropIndex(
                name: "IX_Gifts_EventId",
                table: "Gifts");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "Gifts");
        }
    }
}
