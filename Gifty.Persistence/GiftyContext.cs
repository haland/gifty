﻿using Gifty.Models;
using Microsoft.EntityFrameworkCore;

namespace Gifty
{
    public class GiftyContext : DbContext
    {
        public DbSet<Gift> Gifts { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Person> Persons { get; set; }

        public DbSet<DonorGift> DonorGifts { get; set; }
        public DbSet<RecipientGift> RecipientGifts { get; set; }

        public GiftyContext(DbContextOptions<GiftyContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<DonorGift>()
                .HasKey(dg => new { dg.DonorId, dg.GiftId });

            modelBuilder.Entity<DonorGift>()
                .HasOne(dg => dg.Donor)
                .WithMany(d => d.Donated)
                .HasForeignKey(dg => dg.GiftId);

            modelBuilder.Entity<DonorGift>()
                .HasOne(dg => dg.Donor)
                .WithMany(g => g.Donated)
                .HasForeignKey(dg => dg.DonorId);

            modelBuilder.Entity<RecipientGift>()
                .HasKey(rg => new { rg.RecipientId, rg.GiftId });

            modelBuilder.Entity<RecipientGift>()
                .HasOne(rg => rg.Recipient)
                .WithMany(d => d.Received)
                .HasForeignKey(rg => rg.GiftId);

            modelBuilder.Entity<RecipientGift>()
                .HasOne(rg => rg.Recipient)
                .WithMany(g => g.Received)
                .HasForeignKey(rg => rg.RecipientId);
        }
    }
}
