﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gifty.Models
{
    public class Person
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }

        [InverseProperty("Donor")]
        public List<DonorGift> Donated { get; set; } = new List<DonorGift>();

        [InverseProperty("Recipient")]
        public List<RecipientGift> Received { get; set; } = new List<RecipientGift>();
    }
}
