﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gifty.Models
{
    public class RecipientGift
    {
        [ForeignKey("RecipientId")]
        public Person Recipient { get; set; }
        public Guid RecipientId { get; set; }

        [ForeignKey("GiftId")]
        public Gift Gift { get; set; }
        public Guid GiftId { get; set; }
    }
}
