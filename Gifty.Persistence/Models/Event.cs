﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Gifty.Models
{
    public class Event
    {
        [Key]
        public Guid Id { get; set; }
        public string Description { get; set; }

        public List<Gift> Gifts { get; set; }
    }
}
