﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gifty.Models
{
    public class Gift
    {
        [Key]
        public Guid Id { get; set; }
        public string Description { get; set; }

        public Guid? EventId { get; set; }
        [ForeignKey("EventId")]
        public Event Event { get; set; }

        public List<DonorGift> Donors { get; set; } = new List<DonorGift>();
        public List<RecipientGift> Recipients { get; set; } = new List<RecipientGift>();
    }
}
