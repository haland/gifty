﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Gifty.Models
{
    public class DonorGift
    {
        [ForeignKey("DonorId")]
        public Person Donor { get; set; }
        public Guid DonorId { get; set; }

        [ForeignKey("GiftId")]
        public Gift Gift { get; set; }
        public Guid GiftId { get; set; }
    }
}
