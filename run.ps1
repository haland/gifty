function Invoke-BuildApplications {
    param([string]$app)
    Write-Host "Starting to build projects" -ForegroundColor DarkYellow
    
    $build = {
        param([string]$project)
        & dotnet.exe build $project
    }
    
    $job = Start-Job $build -ArgumentList $app
    
    try {
        while ($job.HasMoreData -and $job.State -eq "Running") {
            Receive-Job -Job $job
        }
    }
    finally {
        if ($job.Error) {
            throw "Build error"
        }

        $job | Stop-Job
    }
    
    Write-Host "Finished building projects" -ForegroundColor DarkGreen
}

function Invoke-RunApplications {
    param([string]$app)
    Write-Host "Starting applications" -ForegroundColor DarkYellow

    $run = {
        param([string]$project)
        & dotnet.exe run --project "$project"
    }
    
    $job = Start-Job $run -ArgumentList $app
    
    try {
        while ($job.HasMoreData -and $job.State -eq "Running") {
            Receive-Job -Job $job
        }
    }
    finally {
        $job | Stop-Job
    }
}

$app = "$PSScriptRoot\Gifty\Gifty.csproj"

Invoke-BuildApplications
Invoke-RunApplications