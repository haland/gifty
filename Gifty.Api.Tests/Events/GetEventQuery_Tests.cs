﻿using Gifty.Api.Events;
using Gifty.Contracts.Events;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Gifts
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class GetEventQuery_Tests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new GetEventQuery(context, ConsoleLogger.Instance<GetEventQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Non_existing_id_returns_not_found_result()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new GetEventQuery(context, ConsoleLogger.Instance<GetEventQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<NotFoundResult>(response);
            }
        }

        [Test]
        public async Task Get_entity_and_map_data_correctly_to_dto()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var @event = context.Events.Add(new Event { Description = "Christmas" }).Entity;
                var gift = context.Gifts.Add(new Gift { Description = "Sock", Event = @event }).Entity;

                context.SaveChanges();

                var sut = new GetEventQuery(context, ConsoleLogger.Instance<GetEventQuery>());

                var response = await sut.Get(@event.Id);

                Assert.IsInstanceOf<OkObjectResult>(response);
                var result = response as OkObjectResult;

                Assert.IsInstanceOf<GetEventDto>(result.Value);
                var dto = result.Value as GetEventDto;

                Assert.Multiple(() =>
                {
                    Assert.That(dto.Id, Is.EqualTo(@event.Id));
                    StringAssert.AreEqualIgnoringCase(dto.Description, "Christmas");

                    Assert.That(dto.Gifts[0].Id, Is.EqualTo(gift.Id));
                });
            }
        }
    }
}
