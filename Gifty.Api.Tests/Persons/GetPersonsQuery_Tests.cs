﻿using Gifty.Contracts.Persons;
using Gifty.Api.Persons;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Persons
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class GetPersonsQuery_Tests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new GetPersonsQuery(context, ConsoleLogger.Instance<GetPersonsQuery>());

                var response = await sut.Get();
                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task No_entities_returns_ok_result_with_empty_list()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new GetPersonsQuery(context, ConsoleLogger.Instance<GetPersonsQuery>());

                var response = await sut.Get();

                Assert.IsInstanceOf<OkObjectResult>(response);
                var result = response as OkObjectResult;

                Assert.IsInstanceOf<GetPersonsDto>(result.Value);
                var dto = result.Value as GetPersonsDto;

                Assert.Multiple(() =>
                {
                    Assert.That(dto, Is.Not.Null);
                    CollectionAssert.IsEmpty(dto.Persons);
                });
            }
        }

        [Test]
        public async Task Returns_ok_result_with_all_entities()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                context.Persons.Add(new Person());
                context.Persons.Add(new Person());
                context.SaveChanges();

                var sut = new GetPersonsQuery(context, ConsoleLogger.Instance<GetPersonsQuery>());

                var response = await sut.Get();

                Assert.IsInstanceOf<OkObjectResult>(response);
                var result = response as OkObjectResult;

                Assert.IsInstanceOf<GetPersonsDto>(result.Value);
                var dto = result.Value as GetPersonsDto;

                Assert.Multiple(() =>
                {
                    Assert.That(dto, Is.Not.Null);
                    Assert.That(dto.Persons.Count, Is.EqualTo(2));
                });
            }
        }
    }
}
