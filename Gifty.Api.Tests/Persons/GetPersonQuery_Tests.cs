﻿using Gifty.Contracts.Persons;
using Gifty.Api.Persons;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Persons
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class GetPersonQuery_Tests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new GetPersonQuery(context, ConsoleLogger.Instance<GetPersonQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Non_existing_id_returns_not_found_result()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new GetPersonQuery(context, ConsoleLogger.Instance<GetPersonQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<NotFoundResult>(response);
            }
        }

        [Test]
        public async Task Getting_returns_ok_result_with_requested_entity()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var id = context.Persons.Add(new Person { Name = "River" }).Entity.Id;
                context.SaveChanges();

                var sut = new GetPersonQuery(context, ConsoleLogger.Instance<GetPersonQuery>());

                var response = await sut.Get(id);

                Assert.IsInstanceOf<OkObjectResult>(response);
                var result = response as OkObjectResult;

                Assert.IsInstanceOf<GetPersonDto>(result.Value);
                var dto = result.Value as GetPersonDto;

                Assert.Multiple(() =>
                {
                    Assert.That(dto, Is.Not.Null);
                    Assert.That(dto.Id, Is.EqualTo(id));
                    Assert.That(dto.Name, Is.EqualTo("River"));
                });
            }
        }
    }
}
