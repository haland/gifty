﻿using Gifty.Contracts.Persons;
using Gifty.Api.Persons;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Persons
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class UpsertPersonCommand_UpdateTests
    {
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto
                {
                    Id = Guid.NewGuid()
                });

                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Non_existing_id_returns_not_found_result()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto
                {
                    Id = Guid.NewGuid()
                });

                Assert.IsInstanceOf<NotFoundResult>(response);
            }
        }
        
        [Test]
        public async Task Updating_maps_properties_to_entity_correctly()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var person = context.Persons.Add(new Person
                {
                    Name = "Hagrid"
                });

                context.SaveChanges();

                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto
                {
                    Id = person.Entity.Id,
                    Name = "Dirgah"
                }) as OkResult;

                Assert.That(response, Is.Not.Null);

                var entity = context.Persons.SingleOrDefault(p => p.Id.Equals(person.Entity.Id));
                Assert.Multiple(() =>
                {
                    Assert.That(entity.Id, Is.EqualTo(person.Entity.Id));
                    Assert.That(entity.Name, Is.EqualTo("Dirgah"));
                });
            }
        }
    }
}
