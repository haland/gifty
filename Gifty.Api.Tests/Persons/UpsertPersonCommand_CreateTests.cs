﻿using Gifty.Contracts.Persons;
using Gifty.Api.Persons;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Persons
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class UpsertPersonCommand_CreateTests
    {
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto());
                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Creating_returns_created_result()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto());

                Assert.That(response, Is.Not.Null);
                Assert.IsInstanceOf<CreatedResult>(response);
            }
        }

        [Test]
        public async Task Creating_maps_properties_to_entity_correctly()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new UpsertPersonCommand(context, ConsoleLogger.Instance<UpsertPersonCommand>());

                var response = await sut.Post(new UpsertPersonDto
                {
                    Name = "Harry Potter"
                }) as CreatedResult;

                Assert.That(response, Is.Not.Null);

                var value = response.Value as CreatedPersonDto;
                Assert.That(value, Is.Not.Null);

                var entity = context.Persons.SingleOrDefault(p => p.Id.Equals(value.Id));
                Assert.Multiple(() =>
                {
                    Assert.That(entity.Id, Is.EqualTo(value.Id));
                    Assert.That(entity.Name, Is.EqualTo("Harry Potter"));
                });
            }
        }
    }
}
