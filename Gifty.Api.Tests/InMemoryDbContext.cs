using Gifty;
using Microsoft.EntityFrameworkCore;
using System;

namespace Gifty.Api.Tests
{
    public sealed class InMemoryDbContext : GiftyContext
    {
        public static InMemoryDbContext Instance(bool forceConnectionError = false) => new InMemoryDbContext(forceConnectionError);

        public InMemoryDbContext(bool forceConnectionError = false) : 
            base(forceConnectionError ? ConnectionErrorDbContextOptions : DbContextOptions)
        {
        }

        private static DbContextOptions<GiftyContext> DbContextOptions =>
            new DbContextOptionsBuilder<GiftyContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

        private static DbContextOptions<GiftyContext> ConnectionErrorDbContextOptions =>
            new DbContextOptionsBuilder<GiftyContext>()
            .Options;
    }
}