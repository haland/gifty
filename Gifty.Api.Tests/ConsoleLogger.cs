﻿using Microsoft.Extensions.Logging;
using NUnit.Framework;
using System;

namespace Gifty.Api.Tests
{
    public static class ConsoleLogger
    {
        public static ILogger<T> Instance<T>() => new NunitLogger<T>();

        public sealed class NunitLogger<T> : ILogger<T>, IDisposable
        {
            private readonly Action<string> output = TestContext.WriteLine;

            public bool IsEnabled(LogLevel logLevel) => true;
            public IDisposable BeginScope<TState>(TState state) => this;

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
                Func<TState, Exception, string> formatter) => output(formatter(state, exception));

            public void Dispose()
            {
            }
        }
    }
}
