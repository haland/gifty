﻿using Gifty.Models;
using NUnit.Framework;
using System;

namespace Gifty.Api.Tests.Bases
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class InMemoryDbContext_Tests 
    {
        [Test]
        public void Can_create_in_memory_database_instance()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                StringAssert.AreEqualIgnoringCase("Microsoft.EntityFrameworkCore.InMemory", context.Database.ProviderName);
            }
        }

        [Test]
        public void Can_add_data_to_in_memory_database()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var person = context.Persons.Add(new Person
                {
                    Name = "Malcolm Reynolds"
                });

                context.SaveChanges();
                Assert.AreNotEqual(Guid.Empty, person.Entity.Id);
            }
        }
    }
}
