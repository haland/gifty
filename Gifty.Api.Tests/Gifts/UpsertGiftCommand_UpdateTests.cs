﻿using Gifty.Contracts.Gifts;
using Gifty.Api.Gifts;
using Gifty.Models;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Gifts
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class UpsertGiftCommand_UpdateTests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new UpsertGiftCommand(context, ConsoleLogger.Instance<UpsertGiftCommand>());

                var response = await sut.Post(new UpsertGiftDto
                {
                    Id = Guid.NewGuid()
                });

                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Should_update_gift_properties()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var entity = context.Gifts.Add(new Gift
                {
                    Description = "Hat"
                }).Entity;

                context.SaveChanges();

                var sut = new UpsertGiftCommand(context, ConsoleLogger.Instance<UpsertGiftCommand>());

                await sut.Post(new UpsertGiftDto
                {
                    Id = entity.Id,
                    Description = "Jayne's hat"
                });

                var gift = context.Gifts.Single(g => g.Id.Equals(entity.Id));
                StringAssert.AreEqualIgnoringCase(gift.Description, "Jayne's hat");
            }
        }
    }
}
