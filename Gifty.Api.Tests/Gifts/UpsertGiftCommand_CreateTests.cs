﻿using Gifty.Contracts.Gifts;
using Gifty.Api.Gifts;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Gifts
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class UpsertGiftCommand_CreateTests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new UpsertGiftCommand(context, ConsoleLogger.Instance<UpsertGiftCommand>());

                var response = await sut.Post(new UpsertGiftDto());

                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Should_update_gift_properties()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new UpsertGiftCommand(context, ConsoleLogger.Instance<UpsertGiftCommand>());

                var response = await sut.Post(new UpsertGiftDto
                {
                    Description = "Jayne's hat"
                });

                Assert.IsInstanceOf<CreatedResult>(response);

                var result = response as CreatedResult;

                Assert.IsInstanceOf<CreatedGiftDto>(result.Value);
                var dto = result.Value as CreatedGiftDto;

                var gift = context.Gifts.FirstOrDefault(g => g.Id.Equals(dto.Id));
                StringAssert.AreEqualIgnoringCase(gift.Description, "Jayne's hat");
            }
        }
    }
}
