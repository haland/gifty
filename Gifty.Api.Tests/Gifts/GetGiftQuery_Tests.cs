﻿using Gifty.Contracts.Gifts;
using Gifty.Api.Gifts;
using Gifty.Models;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Gifty.Api.Tests.Gifts
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class GetGiftQuery_Tests
    {
        [Test]
        public async Task Exception_returns_internal_server_error_result()
        {
            using (var context = InMemoryDbContext.Instance(forceConnectionError: true))
            {
                var sut = new GetGiftQuery(context, ConsoleLogger.Instance<GetGiftQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<InternalServerErrorResult>(response);
            }
        }

        [Test]
        public async Task Non_existing_id_returns_not_found_result()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var sut = new GetGiftQuery(context, ConsoleLogger.Instance<GetGiftQuery>());

                var response = await sut.Get(Guid.NewGuid());
                Assert.IsInstanceOf<NotFoundResult>(response);
            }
        }

        [Test]
        public async Task Get_entity_and_map_data_correctly_to_dto()
        {
            using (var context = InMemoryDbContext.Instance())
            {
                var gift = context.Gifts.Add(new Gift { Description = "Sock" }).Entity;
                var donor = context.Persons.Add(new Person { Name = "Harry Potter" }).Entity;
                var recipient = context.Persons.Add(new Person { Name = "Dobby" }).Entity;

                context.DonorGifts.Add(new DonorGift { Donor = donor, Gift = gift });
                context.RecipientGifts.Add(new RecipientGift { Recipient = recipient, Gift = gift });
                context.SaveChanges();

                var sut = new GetGiftQuery(context, ConsoleLogger.Instance<GetGiftQuery>());

                var response = await sut.Get(gift.Id);

                Assert.IsInstanceOf<OkObjectResult>(response);
                var result = response as OkObjectResult;

                Assert.IsInstanceOf<GetGiftDto>(result.Value);
                var dto = result.Value as GetGiftDto;

                Assert.Multiple(() =>
                {
                    Assert.That(dto.Id, Is.EqualTo(gift.Id));
                    StringAssert.AreEqualIgnoringCase(dto.Description, "Sock");

                    Assert.That(dto.Donors[0].Id, Is.EqualTo(donor.Id));
                    StringAssert.AreEqualIgnoringCase(dto.Donors[0].Name, "Harry Potter");

                    Assert.That(dto.Recipients[0].Id, Is.EqualTo(recipient.Id));
                    StringAssert.AreEqualIgnoringCase(dto.Recipients[0].Name, "Dobby");
                });
            }
        }
    }
}
