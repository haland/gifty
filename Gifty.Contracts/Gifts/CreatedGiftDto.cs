﻿using System;

namespace Gifty.Contracts.Gifts
{
    public class CreatedGiftDto
    {
        public Guid Id { get; set; }
    }
}
