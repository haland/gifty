﻿using System;
using System.Collections.Generic;

namespace Gifty.Contracts.Gifts
{
    public class GetGiftDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }

        public List<GetGift_DonorDto> Donors { get; set; } = new List<GetGift_DonorDto>();
        public List<GetGift_RecipientDto> Recipients { get; set; } = new List<GetGift_RecipientDto>();
    }

    public class GetGift_DonorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class GetGift_RecipientDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
