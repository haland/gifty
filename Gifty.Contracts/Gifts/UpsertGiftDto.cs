﻿using System;
using System.Collections.Generic;

namespace Gifty.Contracts.Gifts
{
    public class UpsertGiftDto
    {
        public Guid? Id { get; set; }
        public string Description { get; set; }

        public List<UpsertGift_DonorDto> Donors { get; set; } = new List<UpsertGift_DonorDto>();
        public List<UpsertGift_RecipientDto> Recipients { get; set; } = new List<UpsertGift_RecipientDto>();
    }

    public class UpsertGift_DonorDto
    {
        public Guid Id { get; set; }
    }

    public class UpsertGift_RecipientDto
    {
        public Guid Id { get; set; }
    }
}
