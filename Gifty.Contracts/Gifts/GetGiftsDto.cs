﻿using System;
using System.Collections.Generic;

namespace Gifty.Contracts.Gifts
{
    public class GetGiftsDto
    {
        public List<GetGifts_GiftDto> Gifts { get; set; } = new List<GetGifts_GiftDto>();
    }

    public class GetGifts_GiftDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }

        public List<GetGifts_DonorDto> Donors { get; set; } = new List<GetGifts_DonorDto>();
        public List<GetGifts_RecipientDto> Recipients { get; set; } = new List<GetGifts_RecipientDto>();
    }

    public class GetGifts_DonorDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class GetGifts_RecipientDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
