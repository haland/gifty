﻿namespace Gifty.Contracts.Up
{
    public class UpDto
    {
        public bool Up { get; } = true;
    }
}
