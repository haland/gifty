﻿using System;

namespace Gifty.Contracts.Persons
{
    public class GetPersonDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
