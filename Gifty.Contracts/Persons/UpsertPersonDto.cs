﻿using System;

namespace Gifty.Contracts.Persons
{
    public class UpsertPersonDto
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
    }
}
