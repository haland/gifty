﻿using System;

namespace Gifty.Contracts.Persons
{
    public class CreatedPersonDto
    {
        public Guid Id { get; set; }
    }
}
