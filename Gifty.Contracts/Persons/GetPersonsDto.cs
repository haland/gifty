﻿using System;
using System.Collections.Generic;

namespace Gifty.Contracts.Persons
{
    public class GetPersonsDto
    {
        public List<GetPersons_PersonDto> Persons { get; set; } = new List<GetPersons_PersonDto>();
    }

    public class GetPersons_PersonDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
