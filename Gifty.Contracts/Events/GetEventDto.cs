﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gifty.Contracts.Events
{
    public class GetEventDto
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public List<GetEvent_GiftDto> Gifts { get; set; }
    }

    public class GetEvent_GiftDto
    {
        public Guid Id { get; set; }
    }
}
